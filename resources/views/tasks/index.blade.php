@extends('layouts.app')
@section('content')
<h1>This is your tasks list</h1>
 
    @if(Request::is('tasks'))
 <a href="{{action('TaskController@my_tasks')}}">my task</a>
    @else <a href="{{action('TaskController@index')}}">all task</a>
    @endif
 <table class = 'table'>
 <thead>  
      <tr>  
      <th>title</th> 
      <th>edit</th> 
      <th>mark</th>
      @cannot('user')   <th>delete</th> @endcannot
      </tr>  
  </thead>
     
     @foreach($tasks as $task)
         <tr>
             <td>{{$task->title }}</td>
             <td><a href ="{{route('tasks.edit', $task ->id)}}" >update</a></td>
     <td>     @if ($task->status)
                 done!
       @else
       @cannot('user')  <a href="{{route('tasks.change_status', [ $task->id, $task->status ])}}">change status</a>@endcannot
       @endif
                    </td>

            @cannot('user')      <td><form method = 'post' action = "{{action('TaskController@destroy',$task->id )}}">
    @csrf 
    @method('DELETE')
    <div class = "form-group">
        <input type = "submit" class = "form-control" name = "submit" value = "delete" >
        </div>

</form></td>@endcannot
         </tr>
             @endforeach
</table>
<a href ="{{route('tasks.create')}}"> create a new task </a>

<script>
       $(document).ready(function(){
           $("button").click(function(event){
            console.log(event.target.id)
               $.ajax({
                   
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.value=1, _token:'{{csrf_token()}}'}),
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>





@endsection
